//Thomas Comeau 1934037
package geometry;

public class Circle implements Shape {
	private double radius;
	
	public Circle(double radius) {
		if(radius >= 0) {
			this.radius = radius;
		}
		else {
			this.radius = 0;
		}
	}

	public double getRadius() {
		return radius;
	}
	@Override
	public double getArea() {
		double area = (Math.pow(radius, 2)*Math.PI);
		return area;
	}

	@Override
	public double getPerimeter() {
		double perimeter = (Math.PI*2*radius);
		return perimeter;
	}

}
