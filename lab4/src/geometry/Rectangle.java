//Thomas Comeau 1934037
package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	
	public Rectangle(double length,double width) {
		if(length >= 0) {
			this.length = length;
		}
		else {
			this.length = 0;
		}
		if(width >= 0) {
			this.width = width;
		}
		else {
			this.width = 0;
		}
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getLength() {
		return length;
	}

	@Override
	public double getArea() {
		double area = length*width;
		return area;
	}

	@Override
	public double getPerimeter() {
		double perimeter = length+length+width+width;
		return perimeter;
	}

}
