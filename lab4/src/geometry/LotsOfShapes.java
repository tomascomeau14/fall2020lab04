//Thomas Comeau 1934037
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(2,6);
		shapes[1] = new Rectangle(5,50);
		shapes[2] = new Circle(5);
		shapes[3] = new Circle(10);
		shapes[4] = new Square(10);
		
		for(int i = 0; i < shapes.length;i++) {
			System.out.println("Area: "+shapes[i].getArea()+", Perimeter: "+shapes[i].getPerimeter());
		}
	}

}
