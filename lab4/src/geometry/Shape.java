//Thomas Comeau 1934037
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}
