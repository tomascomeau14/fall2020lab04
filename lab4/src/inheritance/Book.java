//Thomas Comeau 1934037
package inheritance;

public class Book {
	protected String title;
	private String author;
	
	public Book(String titl, String auth) {
		title = titl;
		author = auth;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public String toString() {
		return ("Title: "+title+", Author: "+author);
	}
	
}