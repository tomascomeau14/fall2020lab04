//Thomas Comeau 1934037
package inheritance;
public class BookStore {

	public static void main(String[] args) {
		Book[] book = new Book[5];
		book[0] = new Book("5 ways to cook","Timmy Wilms");
		book[1] = new ElectronicBook(500,"How to murder","Jim Pickens");
		book[2] = new Book("Perfection of Sorrow","Mr.Depression");
		book[3] = new ElectronicBook(200,"Suitors of the evening","Riley Robson");
		book[4] = new ElectronicBook(6000,"Knights and Nymphs","Jody Apple");
		
		for(int i = 0;i < book.length;i++) {
			System.out.println(book[i].toString());
		}
	}

}
