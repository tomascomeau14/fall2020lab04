//Thomas Comeau 1934037
package inheritance;
public class ElectronicBook extends Book {
	private int numberBytes;
	
	public ElectronicBook(int numb,String titl, String auth) {
		super(titl, auth);
		numberBytes = numb;
	}
	
	public String toString() {
		return("Title: "+getTitle()+", Author: "+getAuthor()+", Number of bytes: "+numberBytes);
	}
}
